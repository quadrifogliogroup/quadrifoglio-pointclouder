#include "ros/ros.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <laser_geometry/laser_geometry.h>
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud2.h"
#include "quadrifoglio_msgs/ultrasound.h"

#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e


class ScanProjector{
    public:
    ScanProjector(ros::Publisher* cloudPub, ros::Publisher* flPub, ros::Publisher* frPub,
    ros::Publisher* fPub, ros::Publisher* rlPub, ros::Publisher* rrPub, ros::Publisher* rPub,
    sensor_msgs::PointCloud2* cloudMsg){ //Message by reference
        _cloudPub = cloudPub;
        _flPub = flPub;
        _frPub = frPub;
        _fPub = fPub;
        _rlPub = rlPub;
        _rrPub = rrPub;
        _rPub = rPub;
        _cloudMsg = cloudMsg;
        _flCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _frCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _fCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _rlCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _rrCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _rCloud = (new pcl::PointCloud<pcl::PointXYZ>);
    }
    ~ScanProjector(){
        delete _flCloud; delete _frCloud; delete _fCloud;
        delete _rlCloud; delete _rlCloud; delete _rCloud;
    }
    void ScanCallback(const sensor_msgs::LaserScan::ConstPtr& scanMsg){
        _projector.projectLaser(*scanMsg, *_cloudMsg);
        _cloudMsg->header.frame_id = "lidar";
        _cloudPub->publish(*_cloudMsg);
    }
    void UltraCallback(const quadrifoglio_msgs::ultrasound::ConstPtr& ultraMsg){

        //Publish data from front cluster
        
        if(ultraMsg->frontWhich == 2){  //Front left
            _flCloud->clear();
            _flCloud->header.frame_id = "fl_ultra";
            _flCloud->is_dense = false;
            if(ultraMsg->frontCm>2){
                _flCloud->width = _flCloud->height = 1;
                _flCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->frontCm)/100.0, 0.0, 0.0));
            }
            pcl_conversions::toPCL(ros::Time::now(), _flCloud->header.stamp);
            _flPub->publish(*_flCloud);
            //ROS_INFO("Publishing FL pcl, width: %d", (int)_flCloud->width);
        }
        else if(ultraMsg->frontWhich == 3){  //Front right
            _frCloud->clear();
            _frCloud->header.frame_id = "fr_ultra";
            _frCloud->is_dense = false;
            if(ultraMsg->frontCm>2){
                _frCloud->width = _frCloud->height = 1;
                _frCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->frontCm)/100.0, 0.0, 0.0));
            }
            pcl_conversions::toPCL(ros::Time::now(), _frCloud->header.stamp);
            _frPub->publish(*_frCloud);
            //ROS_INFO("Publishing FR pcl, width: %d", (int)_frCloud->width);
        }
        else if(ultraMsg->frontWhich == 1){  //Front
            _fCloud->clear();
            _fCloud->header.frame_id = "f_ultra";
            _fCloud->is_dense = false;
            if(ultraMsg->frontCm>2){
                _fCloud->width = _fCloud->height = 1;
                _fCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->frontCm)/100.0, 0.0, 0.0));
            }           
            pcl_conversions::toPCL(ros::Time::now(), _fCloud->header.stamp);
            _fPub->publish(*_fCloud);
            //ROS_INFO("Publishing F pcl, width: %d", (int)_fCloud->width);
        }
        //Publish data from rear cluster

        if(ultraMsg->rearWhich == 3){  //Rear left
            _rlCloud->clear();
            _rlCloud->header.frame_id = "rl_ultra";
            _rlCloud->is_dense = false;
            if(ultraMsg->rearCm>2){
                _rlCloud->width = _rrCloud->height = 1;
                _rlCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->rearCm)/100.0, 0.0, 0.0));
            }
            pcl_conversions::toPCL(ros::Time::now(), _rlCloud->header.stamp);
            _rlPub->publish(*_rlCloud);
            //ROS_INFO("Publishing RL pcl, width: %d", (int)_rlCloud->width);
        }
        else if(ultraMsg->rearWhich == 2){  //Rear right
            _rrCloud->clear();
            _rrCloud->header.frame_id = "rr_ultra";
            _rrCloud->is_dense = false;
            if(ultraMsg->rearCm>2){
                _rrCloud->width = _rrCloud->height = 1;
                _rrCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->rearCm)/100.0, 0.0, 0.0));
            }
            pcl_conversions::toPCL(ros::Time::now(), _rrCloud->header.stamp);
            _rrPub->publish(*_rrCloud);
            //ROS_INFO("Publishing RR pcl, width: %d", (int)_rrCloud->width);
        }
        else if(ultraMsg->rearWhich == 1){  //Rear
            _rCloud->clear();
            _rCloud->header.frame_id = "r_ultra";
            _rCloud->is_dense = false;
            if(ultraMsg->rearCm>2){
                _rCloud->width = _rCloud->height = 1;
                _rCloud->points.push_back(pcl::PointXYZ(((float)ultraMsg->rearCm)/100.0, 0.0, 0.0));
            }           
            pcl_conversions::toPCL(ros::Time::now(), _rCloud->header.stamp);
            _rPub->publish(*_rCloud);
            //ROS_INFO("Publishing R pcl, width: %d", (int)_rCloud->width);
        }


        


        
    }

    private:
    ros::Publisher* _cloudPub;
    ros::Publisher* _flPub;
    ros::Publisher* _frPub;
    ros::Publisher* _fPub;
    ros::Publisher* _rlPub;
    ros::Publisher* _rrPub;
    ros::Publisher* _rPub;
    sensor_msgs::PointCloud2* _cloudMsg;
    laser_geometry::LaserProjection _projector;
    pcl::PointCloud<pcl::PointXYZ>* _flCloud;
    pcl::PointCloud<pcl::PointXYZ>* _frCloud;
    pcl::PointCloud<pcl::PointXYZ>* _fCloud;
    pcl::PointCloud<pcl::PointXYZ>* _rlCloud;
    pcl::PointCloud<pcl::PointXYZ>* _rrCloud;
    pcl::PointCloud<pcl::PointXYZ>* _rCloud;
};


int main(int argc, char **argv){

    ros::init(argc, argv, "quadrifoglio_pointclouder");
    ros::NodeHandle n;


    ros::Publisher cloudPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_lidar", 1);

    ros::Publisher flPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_fl",1);
    ros::Publisher frPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_fr",1);
    ros::Publisher fPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_f",1);
    ros::Publisher rlPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_rl",1);
    ros::Publisher rrPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_rr",1);
    ros::Publisher rPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_ultra_r",1);

    sensor_msgs::PointCloud2 cloudMsg;

    ScanProjector scanProjector(&cloudPub, &flPub, &frPub, &fPub, &rlPub, &rrPub, &rPub, &cloudMsg);

    ros::Subscriber laserScanSub = n.subscribe("scan", 1, &ScanProjector::ScanCallback, &scanProjector);
    ros::Subscriber ultrasoundSub = n.subscribe("quadrifoglio/ultrasound",1,&ScanProjector::UltraCallback, &scanProjector);
    
    ros::spin();

    return 0;
}
